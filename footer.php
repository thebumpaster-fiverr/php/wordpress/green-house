<!-- footer -->
        <footer role="footer">
            
            <!-- logo -->
                <h1>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Apartmani Kartal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" title="Apartmani Kartal" alt="Apartmani Kartal"/></a>
                </h1>
            
            <!-- nav -->
            <?php
			wp_nav_menu( array( 
			  'theme_location' => 'footer-menu',
			  'container' => 'nav',
			  'container_class' => '',
			  )); 
			?>

            <ul role="social-icons">

            	<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>

            </ul>

            <p class="copy-right">&copy; 2016  Apartmani Kartal | Sva prava zadrzana</p>
			<?php wp_footer(); ?>
        </footer>

        <!-- footer -->
    </body>
</html>