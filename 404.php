<?php get_header(); ?>

        <!-- main -->
        <main role="main-home-wrapper" class="container">
            <div class="row">
			<?php 	$name = get_bloginfo('name'); ?>
	<?php 	$desc = get_bloginfo('description'); ?>
	<?php $email = get_bloginfo('admin_email'); ?>
                
            	<section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
                	<article role="pge-title-content">
                    	<header>
                        	<h2><span><?php echo $name; ?></span> <?php echo $desc; ?></h2>
                        </header>

                        <p><?php echo $email;?></p>
                    </article>
                </section>

                <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 grid">
                	<figure class="effect-oscar">
                    	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/6.jpg" alt="" class="img-responsive"/>
                        <figcaption>
                        	<h2>Apartman <span>Tier 1</span></h2>
							<p>Specialy dedicated to our best.</p>
							<a href="works-details.html">See more</a>
                        </figcaption>
                    </figure>
                </section>
                
                <div class="clearfix"></div> <br /> <br /> <br />   <br /> <br /> <br />
				
               <center><h3>404 Nothing found</h3><p><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></p></center>

            </div>

        </main>
		<?php get_footer(); ?>