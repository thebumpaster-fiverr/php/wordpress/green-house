<?php get_header(); ?>
<!-- main -->
	<?php if(have_posts()) : while(have_posts()): the_post();$content = get_the_content();?>
        <main role="main-inner-wrapper" class="container">

                <!-- work details -->
                	<div class="work-details">
                        <div class="row">

                        	<div class="col-xs-12 col-sm-12 col-md-4">
                            	<header role="work-title">
                                    
                                	<h2><?php the_title(); ?></h2>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>çontact/">Booking <i class="fa fa-external-link" aria-hidden="true"></i></a>

                                </header>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-8">

                            	<section>
                                	<p> <?php echo remove_img_from_content($content); ?>
                                </section>
                            </div>
                        </div>

                        <div class="clearfix"></div>
						
						<div class="work-images grid">
						 <ul class="grid-lod effect-2" id="grid">
							<?php 
							$images = get_the_images($content);
							foreach($images as $image) {
								foreach($image as $img) {
									echo $img;
								}
							}
							?>
                         </ul>
                        </div>
						
                    </div>
            </main>
		<?php endwhile; endif; ?>
<?php get_footer(); ?>
