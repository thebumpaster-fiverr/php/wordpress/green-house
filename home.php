<?php get_header(); ?>

        <!-- main -->
        <main role="main-home-wrapper" class="container">
            <div class="row">
			<?php 	$name = get_bloginfo('name'); ?>
	<?php 	$desc = get_bloginfo('description'); ?>
	<?php $email = get_bloginfo('admin_email'); ?>
                
            	<section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
                	<article role="pge-title-content">
                    	<header>
                        	<h2><span><?php echo $name; ?></span> <?php echo $desc; ?></h2>
                        </header>

                        <p><?php echo $email;?></p>
                    </article>
                </section>

                <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 grid">
                	<figure class="effect-oscar">
                    	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/a.jpg" alt="" class="img-responsive"/>
                        <figcaption>
                        	<h2>Apartman <span>Tier 1</span></h2>
							<p>Specialy dedicated to our best.</p>
							<a href="#">See more</a>
                        </figcaption>
                    </figure>
                </section>
                
                <div class="clearfix"></div> <br /> <br /> <br />   <br /> <br /> <br />
				
                <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 grid">
                	<ul class="grid-lod effect-2" id="grid">
						<?php
							
							$args = array('category_name' => 'left');
							$data = get_posts($args);
					
							if($data): foreach($data as $post): setup_postdata($post);	?>
                    	<li>
                        	<figure class="effect-oscar">
								<?php if(has_post_thumbnail($post->ID)): ?>
								<?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                <img src="<?php echo $the_f_image[0]; ?>" alt="" class="img-responsive"/>
								<?php else: ?>
								<img src="http://placeholder.com/420x300" alt="" class="img-responsive"/>
								<?php endif; ?>
                                <figcaption>
                                        <h2><?php the_title(); ?></h2>
                                        <p><?php $myExcerpt = wp_trim_words( get_the_content(), 20, '' ) ; echo $myExcerpt ; ?></p>
                                        <a href="<?php the_permalink(); ?>">See More</a>
                                </figcaption>
                            </figure>
                        </li>
                        <?php endforeach; else: ?>
						<p> No apartmans published! </p>
						<?php endif; ?>
                    </ul>

                </section>

                

                <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 grid">
                	<ul class="grid-lod effect-2" id="grid">
						<?php
							$arg = array('category_name' => 'right');
							$data = get_posts($arg);
					
							if($data): foreach($data as $post): setup_postdata($post);	?>
                    	<li>
                        	<figure class="effect-oscar">
								<?php if(has_post_thumbnail($post->ID)): ?>
								<?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                <img src="<?php echo $the_f_image[0]; ?>" alt="" class="img-responsive"/>
								<?php else: ?>
								<img src="http://placeholder.com/420x300" alt="" class="img-responsive"/>
								<?php endif; ?>
                                <figcaption>
                                        <h2><?php the_title(); ?></h2>
                                        <p><?php $myExcerpt = wp_trim_words( get_the_content(), 20, '' ) ; echo $myExcerpt ; ?></p>
                                        <a href="<?php the_permalink(); ?>">See More</a>
                                </figcaption>
                            </figure>
                        </li>
                        <?php endforeach; else: ?>
						<p> No apartmans published! </p>
						<?php endif; ?>
                    </ul>
                </section>

                <div class="clearfix"></div>

            </div>

        </main>
		<?php get_footer(); ?>