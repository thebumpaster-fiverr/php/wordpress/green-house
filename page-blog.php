<?php get_header(); ?>
<main role="main-inner-wrapper" class="container">
            <div class="row">
<?php 	$name = get_bloginfo('name'); ?>
	<?php 	$desc = get_bloginfo('description'); ?>
	<?php $email = get_bloginfo('admin_email'); ?>
	 <article role="pge-title-content" class="blog-header">
                    	<header>
                        	<h2><span><?php wp_title();?></span> <?php echo $desc; ?></h2>
                        </header>

                        <p><?php echo $email; ?></p>

                    </article>
            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  ">


					<?php $posts = get_posts(); ?>
					<?php if($posts): foreach($posts as $post): setup_postdata($post);?>
					<div id="grid" class="grid-lod effect-2 col-xs-2 col-sm-3 col-md-4 col-lg-4"> 
                        <li>
                            <section class="blog-content">

                            	<a href="<?php the_permalink();?>">
                                <figure>
                                    <div class="post-date">
                                        <?php echo get_the_date(get_option('date_format')); ?>
                                    </div>
									 <?php if(has_post_thumbnail($post->ID)): ?>
									<?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                    <img src="<?php echo $the_f_image[0]; ?>" alt="" class="img-responsive"/>
									<?php endif; ?>
                                </figure>
                                </a>

                                <article>
    
								   <p><?php $myExcerpt = wp_trim_words( get_the_content(), 8, '' ) ; echo $myExcerpt ; ?></p>
                                </article>

                            </section>
                        </li>
					</div>
					<?php endforeach; endif;?>
                </div>

            </div>

        </main>
<?php get_footer(); ?>
