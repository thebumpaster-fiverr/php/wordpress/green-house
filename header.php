<!doctype html>
<html>
	
	<head>
		<title> Villa Kartal | <?php if(is_home()){ echo "Home"; }else{ wp_title(); } ?> </title>
	<?php wp_head(); ?>
	</head>
	
	<body>
		<div id="loader-wrapper">
			<div id="loader"></div>
		</div>
	
	<header role="header">
		<div id="wpadminbar" class></div>
            	<div class="container">
                    
                	<!-- logo -->
                    	<h1 id="logo-img">
                        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Apartmani Kartal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" title="Apartmani Kartal" alt="Apartmani Kartal"/></a>
                        </h1>
                    
                    <!-- nav -->
                        <?php
						wp_nav_menu( array( 
						  'theme_location' => 'header-menu',
						  'container' => 'nav',
						  'container_class' => 'navy',
						  )); 
						?>                    
                </div>
            </header>