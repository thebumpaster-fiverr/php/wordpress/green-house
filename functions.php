<?php
	add_theme_support( 'post-thumbnails' ); 
	add_filter( 'the_content', 'my_the_content_filter' );
	
	function my_the_content_filter($content) {

	  //$matches will be an array with all images
	  preg_match_all("/<img[^>]+\>/i", $content, $matches);

	  //remove all images form content
	  $content = preg_replace("/<img[^>]+\>/i", "", $content);

	  //append the images to the end of the content
	  foreach($matches as $img) {
		   $content .= $img;
	  }

	  return $content;

	}
	function remove_img_from_content($content) {

	  //remove all images form content
	  $content = preg_replace("/<img[^>]+\>/i", "", $content);

	  return $content;

	}
	function get_the_images($content) {

     //$matches will be an array with all images
     preg_match_all("/<img[^>]+\>/i", $content, $matches);
     return $matches;

	}

	function greenhouse_logo() {
		
		add_theme_support( 'custom-logo', array(
		'height'      => 41,
		'width'       => 66,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
		) );
	}
	add_action('after_setup_theme', 'greenhouse_logo');
	
	function greenhouse_the_custom_logo() {
	
		if ( function_exists( 'the_custom_logo' ) ) {
			the_custom_logo();
		}

	}
	
	function register_greenhouse_menus() {
		register_nav_menus(array(
		  'header-menu' => __( 'Header Menu' ),
		  'footer-menu' => __('Footer menu')
		));
	}
	add_action( 'init', 'register_greenhouse_menus' );

	
	function greenhouse_scripts() {
		
		wp_enqueue_style('Main Stylesheet', get_stylesheet_directory_uri()."/css/style.css");
		wp_enqueue_style('Main Bootstrap', get_stylesheet_directory_uri()."/css/bootstrap.min.css");
		wp_enqueue_style('Main Font-Awesome', get_stylesheet_directory_uri()."/css/font-awesome.min.css");
		wp_enqueue_style('Main Responsive', get_stylesheet_directory_uri()."/css/responsive.css");
		
		wp_enqueue_style('Effects set2', get_stylesheet_directory_uri()."/css/effects/set2.css");
		wp_enqueue_style('Effects normalize', get_stylesheet_directory_uri()."/css/effects/normalize.css");
		wp_enqueue_style('Effects component', get_stylesheet_directory_uri()."/css/effects/component.css");
		
		
		wp_enqueue_script('jQuery', get_stylesheet_directory_uri()."/js/jquery.min.js", false);
		wp_enqueue_script('jQuery nav', get_stylesheet_directory_uri()."/js/nav.js" , false);
		
		wp_enqueue_script('Bootstrap', get_stylesheet_directory_uri()."/js/bootstrap.min.js", false );
		
		wp_enqueue_script('Packery', get_stylesheet_directory_uri()."/js/packery.pkgd.min.js", false);
		wp_enqueue_script('Masonry', get_stylesheet_directory_uri()."/js/effects/masonry.pkgd.min.js", true);
		wp_enqueue_script('Image Loader', get_stylesheet_directory_uri()."/js/effects/imagesloaded.js", true );
		wp_enqueue_script('Classie', get_stylesheet_directory_uri()."/js/effects/classie.js" , true);
		wp_enqueue_script('AnimOnScroll', get_stylesheet_directory_uri()."/js/effects/AnimOnScroll.js", true );
		wp_enqueue_script('Modernizer', get_stylesheet_directory_uri()."/js/effects/modernizr.custom.js", true );
		
		wp_enqueue_script('custom', get_stylesheet_directory_uri()."/js/custom.js", false );
		
		wp_enqueue_script('canvas', get_stylesheet_directory_uri()."/js/html5shiv.js", false);
	}
	add_action( 'wp_enqueue_scripts', 'greenhouse_scripts' );
?>