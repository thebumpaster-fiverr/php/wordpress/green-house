<?php get_header(); ?>
<!-- main -->
        <main id="grid" role="main-inner-wrapper" class="container">
			<?php if(have_posts()) : while(have_posts()): the_post(); $post = get_posts(); $content = get_the_content();?>
                <!-- work details -->
                	<div class="work-details">
                        <div class="row">

                        	<div class="col-xs-12 col-sm-12 col-md-6">
                            	<header role="work-title">
                                    
                                	<h2><?php wp_title();?></h2>
                                    <p><a href="tel:8197654321"><i class="fa fa-phone" aria-hidden="true"></i> +38762744279</a></p>
									<p><a href="mailto:contact@apartmanikartal.com"><i class="fa fa-envelope" aria-hidden="true"></i> contact@apartmanikartal.com</a></p>

                                </header>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">

                            	<article class="about-content">
                                	<p> <?php echo remove_img_from_content($content); ?>
                                </article>
                            </div>
							<div class="clearfix"></div>
							<div class="contat-from-wrapper">

               		  <div id="message"></div>
                   
                                <form method="post" action="php/contactfrom.php" name="cform" id="cform">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                    <input  name="name" id="name" type="text" placeholder="Your name">
                                </div>
                                
                                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                    <input  name="email" id="email" type="email"  placeholder="Your email">
                                </div>
                            </div>

                            <div class="clearfix"></div>
                                    
                            <textarea name="comments" id="comments" cols="" rows="" placeholder="Your message..."></textarea>
                                    
                            <div class="clearfix"></div>

                            <input name="" type="submit" value="Send">

                            <div id="simple-msg"></div>

                        </form>

               </div>
                        </div>

						
                    </div>
					<?php endwhile; endif; ?>
            </main>
<?php get_footer(); ?>
